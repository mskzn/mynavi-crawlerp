package defaultpackage;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingContainer;
import javax.swing.border.EmptyBorder;

import javafx.scene.layout.Pane;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.BoxLayout;

public class Gui extends JFrame{
	
	JFrame frame = new JFrame("MyGui");  
    JPanel panel = new JPanel();      
    JLabel label = new JLabel("MyGuiLabel");
    JTextField textField = new JTextField("Input Key Word");
    
    CrawlerThread crawlerThread;
    int intLastpage;
    int intWIPpage;
        
    //Constructor
    public Gui() {
    	    	
    	panel.setLayout(new BorderLayout());
    	panel.add(label,BorderLayout.SOUTH);
    	
    	crawlerThread=new CrawlerThread();
    	    	    	
    	JButton btnNewButton = new JButton("New button");
    	btnNewButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
							
				crawlerThread.start();				
			}
		});
    	    	
		panel.add(btnNewButton, BorderLayout.CENTER);
    	
    	panel.add(textField,BorderLayout.NORTH);
    	textField.setColumns(10);
    	    	
        frame.getContentPane().add(panel);  
        frame.setSize(419, 300);  
        frame.setLocationRelativeTo(null);  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        frame.setVisible(true); 
        
        //send instance
        sendInstance();
    	
    }
    
    public void sendInstance() {
		crawlerThread.getInstance(this);
    }

	public void UpdateLabel() {
		String str;
		if(intWIPpage!=-1) {
	    str=intLastpage+"のうち"+intWIPpage;
		}else {
		str="終了しました。";
		}
	    label.setText(str);
	}
	
	public void collectlastpage(int i) {
		intLastpage=i;
	}
	
	public void setWIPpage(int intWIPpage) {
		this.intWIPpage=intWIPpage;
	}
	
	public String getKeyword(){
		return textField.getText();
	}



	
	

}
