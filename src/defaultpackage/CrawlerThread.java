package defaultpackage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CrawlerThread extends Thread{
	Gui thisgui;
	int intLastpage;
	String strKeyword;
		
	public void run() {
		//execute crawling.
		try {
			executecrawling(new String[0]);
		} catch (IOException e) {
			//Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void executecrawling(String...args) throws IOException {
				//get keyword from JLabel in Gui.
				System.out.println(thisgui.getKeyword());
		
		
				//crawl the first page.
				String strHttpstring="https://tenshoku.mynavi.jp/list/kw"+thisgui.getKeyword();
								
				Document document = Jsoup.connect(strHttpstring).timeout(0).get();
				fetchdata(document);
							
				//find total page,and send it to Gui.
				String strLastpage=getlastpage(document);
				int i=Integer.parseInt(strLastpage);				
				try {
				intLastpage=i/50+1;
				}catch(NullPointerException e) {
					intLastpage=1;
				}								
				thisgui.collectlastpage(intLastpage);
									
				//check if next page exits,and loop while next page exists.
				int n=2;
					while (findnextpage(document)) {
						String strAddress=strHttpstring+"/pg"+n;
						document = Jsoup.connect(strAddress).timeout(0).get();
						System.out.println("********************************************************************************");
						System.out.println("********************************************************************************");
						System.out.println("Page="+n);
						
						//set WIPpage in GUI.
						thisgui.setWIPpage(n);
						
						//update Label in GUI.
						thisgui.UpdateLabel();
												
						//add random pause.
						Random randomGenerator=new Random();
						int sleepTime=randomGenerator.nextInt(10000);
						System.out.println("milsec="+sleepTime);
						try {
							Thread.sleep(sleepTime);
						} catch (Exception e) {
							// handle exception
						}
						fetchdata(document);
						n=n+1;
					}	
					//finish crawling ,send message.
					//"-1" means that crawling has finished.
					this.thisgui.setWIPpage(-1);
					this.thisgui.UpdateLabel();
					
	}

	public static void sendText(String string) {
		System.out.println(string);
	}

	public static String getlastpage(Document document) {
			
		String retStr=null;
		//find element that has "js__searchRecruit--count"
		Elements elements=document.getElementsByClass("js__searchRecruit--count");
		
		for(Element element:elements) {
			retStr=element.ownText();
		}
					
		return retStr;
		
	}

	public static Boolean findnextpage(Document document) {
		Boolean bfindnextpage=false;		
		
		//detect element that has "a" tag and class="iconFont--arrowLeft".
		Elements elements=document.select("a[class='iconFont--arrowLeft']");
		
		if(elements.isEmpty()) {
			bfindnextpage=false;
		}else {
			bfindnextpage=true;
		}
		return bfindnextpage;
			
	}

	public static void fetchdata(Document document)
	throws IOException {
		
		 // each article is divided by "cassetteRecuruict"tag.
	    Elements elements = document.getElementsByClass("cassetteRecruit");
		
	    //declare a csv file with its path.
	    String csvFilePath=getdesktopPath().replace("/", "\\")+"\\mynaviexporttest.csv";
	               
	    //check if file already exists.
	    File tempfile=new File(csvFilePath);
	    boolean fileExists=tempfile.exists();
	
	    if(!fileExists) {
	    	
	    //if file does not exist,prepare a bufferedwriter.
	    BufferedWriter fileWriter=new BufferedWriter(new FileWriter(csvFilePath));
	    //then write header to csvfile.
	    fileWriter.write("company_name,salary");
	    //once close filewriter
	    fileWriter.close();
	    	
	    }
	    
	    //append data to csv.
	  
	    FileWriter fw=new FileWriter(csvFilePath,true);
	          
	    for (Element element:elements) {
	    	//collect company names.
	    	//company name has "cassetteRecuruit_name"tag.
	    	Elements nameElements=element.getElementsByClass("cassetteRecruit__name");
	       	String strCompanyName=null;
	       	String strSalary=null;
	    	for(Element el:nameElements) {
	    		
	    		String key=" |";
	    		int index=el.ownText().indexOf(key);
	                		
	    		if(index==-1) {
	    			strCompanyName=el.ownText();	
	    			}else {
	    			strCompanyName=el.ownText().substring(0, el.ownText().indexOf(key));
	    			}
	                		
	    			System.out.println(strCompanyName);
	    	}
	    	
	    	try {
	    	//collect salaries.
	    	//salaries are included in tables.
	    	
	    	Elements mainElements=null;
	    	mainElements=element.getElementsByClass("tableCondition");
	    	        	     	
	    	
	    	Elements table=null;
	    	try{
	    			table=mainElements.last().getElementsByTag("table");
	    		}catch (NullPointerException e) {
					// : handle exception
	    		}
	    	
	    	Elements rows=table.select("tr");
	    	
	    	for(int i=0;i<rows.size();i++) {
	    		Element row=rows.get(i);
	    		Elements head=row.select("th");
	    		            		
	    		if(head.text().equals("���^")) {
	    			Elements salary=row.select("td");
	    			strSalary=salary.get(0).text();
	    			//System.out.println(strSalary);
	    				            			
	    		}
	    		
	    	}
	    	}catch(Exception e) {
	    	e.printStackTrace();	
	    	}
	    	
	    	
	    		            	
	    //save result in csv
	
	   strCompanyName=strCompanyName.replaceAll("\"","\"\"");
	   strSalary=strSalary.replaceAll("\"","\"\"");   
	    	
	    	
	   String line=String.format("%s,%s", "\""+strCompanyName+"\"","\""+strSalary+"\""); 	        
	   System.out.println(line); 
	
	
	   fw.append("\n");
	   fw.append(line);
	
	   	               
	}
	
		fw.flush();
		fw.close();
			
		
	}
	
	 public void getInstance(Gui gui) {
	    	thisgui=gui;
	    }
	 

	public void setKeyword(String strKeyword){
			this.strKeyword=strKeyword;
		}
	
	public static String getdesktopPath() {
		String desktopPath=null;
		
		try{
			  desktopPath = System.getProperty("user.home") + "/Desktop";
			  System.out.print(desktopPath.replace("\\", "/"));
			  }catch (Exception e){
			  System.out.println("Exception caught ="+e.getMessage());
			  }
				
		return desktopPath;
	}
}
